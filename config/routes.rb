Rails.application.routes.draw do
  devise_for :users

  namespace :admin do
    get '', to: :index
    put '', to: :update
  end

  get 'home/index'
  root 'home#index'
end
